docker run -d \
  -it \
  --name datalake \
  -p 8088:8088 \
  -p 10002:10002 \
  -p 9000:9000 \
  -p 9870:9870 \
  -v /user/docker/datalake/minio:/tmp/data:cached \
  mdl:2.2 
